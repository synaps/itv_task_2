﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ektaco_task.Models
{
	public class Product
	{
		public int Id { get; set; }		// primary key
		public String Name { get; set; }
		public int GroupID { get; set; }
		public Group Group { get; set; }
		public DateTime DateCreated { get; set; }
		public float Price { get; set; }
		public float PriceVAT { get; set; }
		public float VAT { get; set; }
	}
}
