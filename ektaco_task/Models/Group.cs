﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ektaco_task.Models
{
	public class Group
	{
		public Group()
		{
			ParentGroupId = 1;		// default group
		}

		public int Id { get; set; }		// Primary key
		public String Name { get; set; }
		public int? ParentGroupId { get; set; }
		public Group ParentGroup { get; set; }

		public List<Group> ChildGroups { get; set; }
	}
}
