﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ektaco_task.Models
{
	public class ProductContext : DbContext
	{
		public ProductContext(DbContextOptions<ProductContext> pOptions) : base(pOptions)
		{ }

		public DbSet<Product> Products { get; set; }
		public DbSet<Store> Stores { get; set; }
		public DbSet<Group> Groups { get; set; }

		protected override void OnModelCreating(ModelBuilder pModelBuilder)
		{
			/*pModelBuilder.Entity<Group>()
				.HasOne<Group>(g => g.ParentGroup);*/

			Group gr1 = new Group { Id = 1, Name = "Group 1"};
			Group gr2 = new Group { Id = 2, Name = "Group 2" };
			Group gr3 = new Group { Id = 3, Name = "Sub group 1", ParentGroupId = gr1.Id};
			Group gr4 = new Group { Id = 4, Name = "Sub group 2", ParentGroupId = gr1.Id };
			Group gr5 = new Group { Id = 5, Name = "Sub group 3", ParentGroupId = gr2.Id };
			Group gr6 = new Group { Id = 6, Name = "Sub group 4", ParentGroupId = gr3.Id };

			pModelBuilder.Entity<Group>().HasData(gr1, gr2, gr3, gr4, gr5, gr6);

			/*pModelBuilder.Entity<Product>()
				.HasOne<Group>(g => g.Group_Id);*/

			Product pr1 = new Product { Id = 1, Name = "Sugar", Price = 1.1f, VAT = 20, DateCreated = DateTime.Now, GroupID = gr1.Id };
			Product pr2 = new Product { Id = 2, Name = "Cereals", Price = 2.5f, VAT = 5, DateCreated = DateTime.Now, GroupID = gr1.Id };
			Product pr3 = new Product { Id = 3, Name = "Jam", Price = 4.2f, VAT = 20, DateCreated = DateTime.Now, GroupID = gr2.Id };
			Product pr4 = new Product { Id = 4, Name = "Milk", Price = 2.5f, VAT = 25, DateCreated = DateTime.Now, GroupID = gr2.Id };
			Product pr5 = new Product { Id = 5, Name = "Cheese", Price = 11f, VAT = 20, DateCreated = DateTime.Now, GroupID = gr3.Id };
			Product pr6 = new Product { Id = 6, Name = "Tea", Price = 88.0f, VAT = 15, DateCreated = DateTime.Now, GroupID = gr3.Id };
			Product pr7 = new Product { Id = 7, Name = "Coffee", Price = 89.0f, VAT = 20, DateCreated = DateTime.Now, GroupID = gr4.Id };
			Product pr8 = new Product { Id = 8, Name = "Bread", Price = 57.2f, VAT = 14, DateCreated = DateTime.Now, GroupID = gr4.Id };
			Product pr9 = new Product { Id = 9, Name = "Cookies", Price = 1024.8f, VAT = 20, DateCreated = DateTime.Now, GroupID = gr5.Id };
			Product pr10 = new Product { Id = 10, Name = "Salt", Price = 255.5f, VAT = 75, DateCreated = DateTime.Now, GroupID = gr5.Id };
			Product pr11 = new Product { Id = 11, Name = "Ketchap", Price = 10.3f, VAT = 20, DateCreated = DateTime.Now, GroupID = gr6.Id };
			Product pr12 = new Product { Id = 12, Name = "Cucumber", Price = 0.5f, VAT = 21, DateCreated = DateTime.Now, GroupID = gr6.Id };

			pModelBuilder.Entity<Product>().HasData(pr1, pr2, pr3, pr4, pr5, pr6, pr7, pr8, pr9, pr10, pr11, pr12);

			Store st1 = new Store { Id = 1, Name = "Store 1" };
			Store st2 = new Store { Id = 2, Name = "Store 2" };
			Store st3 = new Store { Id = 3, Name = "Store 3" };

			pModelBuilder.Entity<Store>().HasData(st1, st2, st3);
		}
	}
}
