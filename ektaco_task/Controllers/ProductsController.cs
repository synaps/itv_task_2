﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using ektaco_task.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ektaco_task.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ProductsController : Controller
	{
		private readonly ProductContext _ProductContext;

		public ProductsController(ProductContext pContext)
		{
			_ProductContext = pContext;
		}

		// GET: api/<controller>
		[HttpGet]
		public ActionResult<List<Product>> GetAll()
		{
			List<Product> rProducts = _ProductContext.Products.ToList();
			// FIXME: after inserting nullable type response is not returned
			/*foreach (Product product in rProducts)
			{
				product.Group = _ProductContext.Groups.Find(product.GroupID);
			}*/
			return rProducts;
		}

		// GET api/<controller>/5
		[HttpGet("{id}", Name = "GetProduct")]
		public ActionResult<Product> GetById(int id)
		{
			Product tProduct = _ProductContext.Products.Find(id);
			if (tProduct == null)
				return NotFound();
			return tProduct;
		}

		// POST api/<controller>
		[HttpPost]
		public IActionResult Create(Product pProduct)
		{
			if (pProduct.Price == 0)
			{
				pProduct.Price = pProduct.PriceVAT * 100 / (pProduct.VAT + 100);		// data in %
			}
			else if(pProduct.PriceVAT == 0)
			{
				pProduct.PriceVAT = pProduct.Price * (100 + pProduct.VAT) / 100;
			}
			else
			{
				pProduct.VAT = pProduct.PriceVAT * 100 / pProduct.Price - 100;
			}


			_ProductContext.Add(pProduct);
			_ProductContext.SaveChanges();

			return CreatedAtRoute("GetProduct", new { id = pProduct.Id }, pProduct);
		}
	}
}
