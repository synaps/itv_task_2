﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ektaco_task.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ektaco_task.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class GroupController : Controller
	{
		private readonly ProductContext _ProductContext;

		public GroupController(ProductContext pContext)
		{
			_ProductContext = pContext;
		}

		// GET: api/<controller>/
		[HttpGet]
		public ActionResult<List<Group>> GetAll()
		{
			List<Group> rGroups = _ProductContext.Groups.ToList();
			foreach(Group group in rGroups)
			{

			}
			return rGroups;
		}
	}
}
