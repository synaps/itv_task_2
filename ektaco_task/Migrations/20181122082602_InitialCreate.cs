﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ektaco_task.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    ParentGroupId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Groups_Groups_ParentGroupId",
                        column: x => x.ParentGroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Stores",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stores", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    GroupID = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    Price = table.Column<float>(nullable: false),
                    PriceVAT = table.Column<float>(nullable: false),
                    VAT = table.Column<float>(nullable: false),
                    StoreId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_Groups_GroupID",
                        column: x => x.GroupID,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Products_Stores_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Stores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "Group 1" });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "Id", "Name" },
                values: new object[] { 2, "Group 2" });

            migrationBuilder.InsertData(
                table: "Stores",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "Store 1" });

            migrationBuilder.InsertData(
                table: "Stores",
                columns: new[] { "Id", "Name" },
                values: new object[] { 2, "Store 2" });

            migrationBuilder.InsertData(
                table: "Stores",
                columns: new[] { "Id", "Name" },
                values: new object[] { 3, "Store 3" });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "Id", "Name", "ParentGroupId" },
                values: new object[] { 3, "Sub group 1", 1 });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "Id", "Name", "ParentGroupId" },
                values: new object[] { 4, "Sub group 2", 1 });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "Id", "Name", "ParentGroupId" },
                values: new object[] { 5, "Sub group 3", 2 });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "DateCreated", "GroupID", "Name", "Price", "PriceVAT", "StoreId", "VAT" },
                values: new object[] { 1, new DateTime(2018, 11, 22, 10, 26, 2, 611, DateTimeKind.Local), 1, "Sugar", 1.1f, 0f, null, 20f });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "DateCreated", "GroupID", "Name", "Price", "PriceVAT", "StoreId", "VAT" },
                values: new object[] { 2, new DateTime(2018, 11, 22, 10, 26, 2, 658, DateTimeKind.Local), 1, "Cereals", 2.5f, 0f, null, 5f });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "DateCreated", "GroupID", "Name", "Price", "PriceVAT", "StoreId", "VAT" },
                values: new object[] { 3, new DateTime(2018, 11, 22, 10, 26, 2, 658, DateTimeKind.Local), 2, "Jam", 4.2f, 0f, null, 20f });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "DateCreated", "GroupID", "Name", "Price", "PriceVAT", "StoreId", "VAT" },
                values: new object[] { 4, new DateTime(2018, 11, 22, 10, 26, 2, 658, DateTimeKind.Local), 2, "Milk", 2.5f, 0f, null, 25f });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "Id", "Name", "ParentGroupId" },
                values: new object[] { 6, "Sub group 4", 3 });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "DateCreated", "GroupID", "Name", "Price", "PriceVAT", "StoreId", "VAT" },
                values: new object[] { 5, new DateTime(2018, 11, 22, 10, 26, 2, 658, DateTimeKind.Local), 3, "Cheese", 11f, 0f, null, 20f });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "DateCreated", "GroupID", "Name", "Price", "PriceVAT", "StoreId", "VAT" },
                values: new object[] { 6, new DateTime(2018, 11, 22, 10, 26, 2, 658, DateTimeKind.Local), 3, "Tea", 88f, 0f, null, 15f });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "DateCreated", "GroupID", "Name", "Price", "PriceVAT", "StoreId", "VAT" },
                values: new object[] { 7, new DateTime(2018, 11, 22, 10, 26, 2, 658, DateTimeKind.Local), 4, "Coffee", 89f, 0f, null, 20f });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "DateCreated", "GroupID", "Name", "Price", "PriceVAT", "StoreId", "VAT" },
                values: new object[] { 8, new DateTime(2018, 11, 22, 10, 26, 2, 658, DateTimeKind.Local), 4, "Bread", 57.2f, 0f, null, 14f });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "DateCreated", "GroupID", "Name", "Price", "PriceVAT", "StoreId", "VAT" },
                values: new object[] { 9, new DateTime(2018, 11, 22, 10, 26, 2, 658, DateTimeKind.Local), 5, "Cookies", 1024.8f, 0f, null, 20f });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "DateCreated", "GroupID", "Name", "Price", "PriceVAT", "StoreId", "VAT" },
                values: new object[] { 10, new DateTime(2018, 11, 22, 10, 26, 2, 658, DateTimeKind.Local), 5, "Salt", 255.5f, 0f, null, 75f });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "DateCreated", "GroupID", "Name", "Price", "PriceVAT", "StoreId", "VAT" },
                values: new object[] { 11, new DateTime(2018, 11, 22, 10, 26, 2, 658, DateTimeKind.Local), 6, "Ketchap", 10.3f, 0f, null, 20f });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "DateCreated", "GroupID", "Name", "Price", "PriceVAT", "StoreId", "VAT" },
                values: new object[] { 12, new DateTime(2018, 11, 22, 10, 26, 2, 658, DateTimeKind.Local), 6, "Cucumber", 0.5f, 0f, null, 21f });

            migrationBuilder.CreateIndex(
                name: "IX_Groups_ParentGroupId",
                table: "Groups",
                column: "ParentGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_GroupID",
                table: "Products",
                column: "GroupID");

            migrationBuilder.CreateIndex(
                name: "IX_Products_StoreId",
                table: "Products",
                column: "StoreId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "Stores");
        }
    }
}
