# Inteview task by Rustam Rahimgulov

## Tasks

1. To add product construct JSON and post it to address `https://localhost:44318/api/products`. Sample contents of the request is shown below. Price, VAT, Price with VAT will be calculated before the saving the product to the database.

    {
        "Name": "Phone",
        "Price": 4,
        "VAT": 25,
        "GroupId": 1
    }

2. To view product by ID perform a **GET** request to address https://localhost:44318/api/products/<ID>, where <ID> is the id of the product you would like to receive. If <ID> is not provided then all the Products will be returned.

3. Returning of the groups was not implemented correctly as the Controller does not return a valid response due to nullable type used in the model.

4. Missing on the task sheet.

5. Database can be seeded in two ways. Migrations with command from CMD/terminal: `dotnet ef database update` or another command issued in Power Shell: `Update-Database`. Second way is execute sql script available in the repository to a newly created `ektaco.db` SQLite database. **Model may have changed since the script was created.**

### sqlite
SQLite is used and database is automatically generated with the migrations. 